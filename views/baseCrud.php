<?php echo $this->template->cardOpen('Crud Name');?>

<?php echo $this->template->cardBodyOpen();?>
	<div class="btn-group" id="action_button">
		<button class="btn btn-primary" id="buttonAdd" onclick="addData()">
			<span class="fa fa-plus"></span> Tambah
		</button>
	</div>

	<table class="table table-hover table-bordered" id="tableID">
		<thead>
			<tr>
				<!-- Table Header
					<th class="text-center">ID</th> 
				-->

				

				<!-- end table header -->
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>

<script type="text/javascript">
	$(document).ready(function(){
		// initiate datatable
			$("#tableID").DataTable({
				processing:true,
				serverSide:true,
				ajax : {
					url:"<?php echo site_url('baseCrud/loadList') ?>",
					type:"POST"
				},
				columns :[
					{data:"id"},
					{data:"xs1"},
					{
						data:"xn1",
						render:function(data){
							return format_rp(data);
						}
					},
					{data:"xd1"},
					{data:"xt1"},
					{
						data:"aksi",
						render:function(data,type,row){
							txt = '';

							// button edit
								txt += '<button class="btn btn-primary btn-xs edit"';

								/* attribute
								txt += 'id = "'+row.id+'"'
								*/



								// end attribute

								txt += '>';
								txt += '	<span class="fa fa-pencil"></span> Ubah';
								txt += '</button> ';

							// button delete
								txt += '<button class="btn btn-danger btn-xs delete"';
								
								/* attribute
								txt += 'id = "'+row.id+'"'
								*/



								// end attribute

								txt += '>';
								txt += '	<span class="fa fa-trash"></span> Hapus';
								txt += '</button>';

							return txt;
						}
					},
				],
				columnDefs:[
					// untuk yang rupiah
						{
							targets : 2,
							className :'dt-body-right'
						},
					// untuk aksi
						{
							targets : -1,
							className :'dt-body-center'
						}
				]
			});

			$(document).on('click','.edit',function(){
				/*attribute
					id = $(this).attr('id');
					xs1 = $(this).attr('xs1');
				*/

				/*attribute
					$("#id").val(id);
				*/
				$("#modal-addTitle").text('Ubah data #'+id);

				$("#modal-add").modal('show');
			});

			$(document).on('click','.delete',function(){
				/*Attribute
					id = $(this).attr('id');
					xs1 = $(this).attr('xs1');
				*/


				conf = confirm("Apakah anda yakin ingin menghapus data #"+id+" ?");
				if(conf){
					$.ajax({
						url:"<?php echo site_url('baseCrud/deleteData') ?>",
						type :"POST",
						data : {
							id : id
						},
						success:function(res){
							res = JSON.parse(res);
							if(res.code == 200){
								custom_notification('success',res.info);
								refreshTable();
							}else{
								custom_notification('danger',res.info);
							}
						}
					});
				}
			});

			$("#tableID_filter").append($("#action_button"));

		// saveForm
			$("#formAdd").validate({
				rules : {
					/* Attribute
					xs1 : {
						required : true,
					},
					*/
				},
				submitHandler : function(){
					$.ajax({
						url:"<?php echo site_url('baseCrud/save') ?>",
						type : "POST",
						data : $("#formAdd").serialize(),
						beforeSend:function(){
							buttonSpinner('btnAddSave');
						},
						success:function(res){
							res = JSON.parse(res);
							if(res.code == 200){
								custom_notification('success',res.info);
								refreshTable();
							}else{
								custom_notification('danger',res.info);
							}
						},
						complete:function(){
							$(".modal").modal('hide');
							removeButtonSpinner('btnAddSave');
						}
					});
				}

			});
	});

	function refreshTable(){
		$("#tableID").DataTable().ajax.reload();
	}

	function addData(){
		$("#modal-addTitle").text('Tambah data baru');
		$("#id").val(0);
		$("#formAdd")[0].reset();
		$("#modal-add").modal('show');
	}
</script>
<div id="modal-add" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-addTitle">Tambah Crud name</h4>
            </div>
            <div class="modal-body">
            	<form method="POST" action="#" class="form-horizontal" id="formAdd">
            		
            	</form>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick='$("#formAdd").submit()' id="btnAddSave">
            		<span class="fa fa-check"></span> Simpan
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>