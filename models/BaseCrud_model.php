<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseCrud_model extends CI_Model {
	protected $_table = 'basecrud';
	protected $_key;

	public function loadList($mode = 'LOAD_ALL',$params = array()){
		// total rows
			$recordsTotal = $this->db->count_all_results($this->_table);

		// search query
			if(isset($params['search']) && $params['search']){
					$this->db->start_cache();

					/* write every coloumn here for searching
					$this->db->or_like('lower(xs1)',strtolower($params['search']),'both');
					*/

					$this->db->stop_cache();
			}

		// pagination query
			if(isset($params['pagination'])){
				$limit = $params['pagination']['limit'];
				$offset = $params['pagination']['offset'];
			}else{
				$limit = 0;
				$offset = 0;
			}

		// ordering query
			if(isset($params['order'])){
				$this->db->order_by($params['order']['column'],$params['order']['dir']);
			}

		// base query
			$data = $this->db->get($this->_table,$limit,$offset);

		// getTotalFiltered
			$recordsFiltered = $this->db->count_all_results($this->_table);

		$result = new stdClass;
		$result->data = $data->result();
		$result->recordsTotal = $recordsTotal;
		$result->recordsFiltered = $recordsFiltered;

		return $result;
	}
	public function save( $params = array() ){
		$CI =& get_instance();
		$CI->load->model('core/general_model');
		if(isset($params['id']) && !empty($params['id'])){
			// update records
			$id = $params['id'];
			$this->db->where('id',$id);
			$this->db->set($params);
			$this->db->update($this->_table);
			$resultData = array(
				'id' => $id,
			);
			$result = $CI->general_model->result(200,'Berhasil Merubah data.',$resultData);
		}else{
			// insert data
			unset($params['id']);
			$params['id'] = $this->getNewCode();

			$this->db->insert($this->_table,$params);
			$resultData = array(
				'insert_id' => $this->db->insert_id(),
			);
			$result = $CI->general_model->result(200,'Berhasil Menambah Data',$resultData);
		}
		return $result;
	}
	public function delete( $params = array() ){
		$this->db->where('id',$params['id']);
		$this->db->delete($this->_table);

		$CI =& get_instance();
		$CI->load->model('general_model');
		$result = $this->general_model->result(200,'Sukses Menghapus Data #'.$params['id']);
		return $result;
	}
	protected function getNewCode(){
		// get row count
			$rowCount = $this->db->count_all_results($this->_table);
			$nextValue = $rowCount + 1;
			$nextValue= str_pad($nextValue, 6,'0',STR_PAD_LEFT);
		$prefix = 'BSC-';
		$newCode = $prefix.$nextValue;

		return $newCode;
	}
}

/* End of file BaseCrud_model.php */
/* Location: ./application/modules/baseCrud/models/BaseCrud_model.php */