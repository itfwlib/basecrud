<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class baseCrud extends Controller{
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		// dipake untuk namain card
		$this->using('datatable');
		$this->using('jquery.validate');
		$this->template->load(DEF_TEMPLATE_INSIDE,'baseCrud',get_defined_vars());	
	}
	public function loadList(){
		$this->load->model('core/general_model');
		$result = $this->general_model->result();
		$params = $this->input->post();
		$dataTable = false;

		if(isset($params['draw'])){
			$dataTable = true;
			$search = $params['search']['value'];
			$pagination = array(
				'limit' => $params['length'],
				'offset' => $params['start']
			);

			$ordering = array(
				'column' => $params['columns'][ $params['order'][0]['column'] ]['data'],
				'dir' => $params['order'][0]['dir']
			);
			
			$loadParams = array(
				'search' => $search,
				'pagination' => $pagination,
				'order' => $ordering
			);

		}else{
			$loadParams = array();
		}
		$this->load->model('baseCrud_model','model');
		$data = $this->model->loadList('LOAD_ALL',$loadParams);

		if($dataTable){
			$result = array(
				'draw' => $params['draw'],
				'recordsTotal' => $data->recordsTotal,
				'recordsFiltered' => $data->recordsFiltered,
				'data' => $data->data
			);
		}else{
			$result->data = $data;
		}
		echo json_encode($result);
	}
	public function save(){
		$params = $this->input->post();

		/*
			sanitizing rupiah dbawah ini
			$params['xn1'] = format_angka($params['xn1']); 
		*/

		$this->form_validation->set_data($params);

		/*
			form_validation_rules dibawah ini
			$this->form_validation->set_rules('xs1', 'XS1', 'required');
			$this->form_validation->set_rules('xn1', 'XN1', 'required');
			$this->form_validation->set_rules('xd1', 'XD1', 'required');
			$this->form_validation->set_rules('xt1', 'xt1', 'required');
		*/

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('BaseCrud_model');
			$result = $this->BaseCrud_model->save($params);
		
		} else {

			$this->load->model('general_model');
			$result = $this->general_model->result(401,validation_errors());

		}
		echo json_encode($result);
	}
	public function deleteData(){
		$params = $this->input->post();
		$this->load->model('baseCrud_model');
		$result = $this->baseCrud_model->delete($params);
		echo json_encode($result);
	}
	function alpha_dash_space($fullname){
	    if (! preg_match('/^[a-zA-Z\s]+$/', $fullname)) {
	        $this->form_validation->set_message('alpha_dash_space', '%s Hanya bisa diisi oleh huruf dan spasi.');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}
}	